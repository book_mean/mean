var gulp = require('gulp')
var livereload = require('gulp-livereload')

var fs = require('fs')
fs.readdirSync (__dirname + '/gulp').forEach( function (task) {
    require('./gulp/' + task)
})

gulp.task('watch:js', ['js'], function() {
    livereload.listen();
    gulp.watch('ng/**/*.js', ['js'])
})

gulp.task('watch:css',['css'], function() {
    livereload.listen();
    gulp.watch('css/**/*.styl', ['css'])
})

gulp.task('dev', ['watch:css', 'watch:js', 'dev:server'])