app.controller('PostsCtrl', function($scope, PostsSvc) {

    PostsSvc.fetch().success (function(posts) {
        $scope.posts = posts
    });

    $scope.addPost = function() {
        if ($scope.postBody) {
            PostsSvc.create({
                username: 'zborcjusz',
                body: $scope.postBody
            }).success(function (post) {
                $scope.posts.unshift(post); // dodanie postu do tablicy (na początek) z postami
                $scope.postBody = null; //usunięcie zawartości pola input
            })
        }
    }
    
})