var gulp = require('gulp')
var stylus = require('gulp-stylus')
var livereload = require('gulp-livereload')

gulp.task('css', function() {
    gulp.src('css/**/*.styl')
        .pipe(stylus({
            compress: true
        }))
        .pipe(gulp.dest('assets'))
        .pipe(livereload({ start: true }));
})